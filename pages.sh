#!/bin/sh

i="$#"

while [ $i -gt 0 ]
do
    case "$1" in
	-i)
	    auth="$2"
	    ;;
	-t)
	    title=$(printf %s "$2" | jq -sR .)
	    ;;
	-n)
	    name=$(printf %s "$2" | jq -sR .) #page url
	    ;;
	-s)
	    summary=$(printf %s "$2" | jq -sR .)
	    ;;
	-c)
	    content=$(printf %s "$2" | jq -sR .)
	    ;;
	-h)
	    host="$2"
	    ;;
	*)
	    echo "syntax error, exiting"
	    exit 1
	    ;;
    esac

    shift 2
    i=$((i-2))
done

# {
#   title: '',
#   name: '',
#   summary: '',
#   content: [],
#   variables: [],
#   script: '',
#   eyeCatchingImageId: '',
#   font: '',
#   alignCenter: false,
#   hideTitleWhenPinned: false,
# }

page=$(printf '{
  "i": "%s",
  "title": %s,
  "name": %s,
  "summary": %s,
  "content": [
      {
	"text": %s,
	"type": "text"
      }
  ],
  "variables": [
      {
	"": ""
      }
  ],
  "script": "" 
}' "$auth" "$title" "$name" "$summary" "$content")

#echo "$page"

curl -X POST \
     --header "Content-Type: application/json; charset=UTF-8" \
     --data "$page" \
     https://"$host"/api/pages/create

printf "\n"
