#!/bin/sh

#echo "$#"

i="$#"

while [ $i -gt 0 ]
do
    #echo "i is $i"
    #echo "1 is $1"
    case "$1" in
	-i)
	    auth="$2"
	    ;;
	-v)
	    visibility="$2"
	    ;;
	-c) #c is for content, we use it instead of -t for consistency with pages
	    text=$(printf %s "$2" | jq -sR .)
	    ;;
	-w)
	    cw="$2"
	    ;;
	-l)
	    localOnly="$2"
	    ;;
	-h)
	    host="$2"
	    ;;
	*)
	    echo "syntax error, exiting"
	    exit 1
	    ;;
    esac

    shift 2
    i=$((i-2))
done

if [ -z "$localOnly" ]
then
    localOnly=false
fi

if [ -z "$visibility" ]
then
    visibility=public
fi

if [ -z "$cw" ]
then
    notecw=""
else
    notecw="\"cw\": \"$cw\","
fi

# {

#     "visibility": "public",
#     "visibleUserIds": 

#     [

# 	"string"

#     ],
#     "text": "string",
#     "cw": "string",
#     "localOnly": false,
#     "noExtractMentions": false,
#     "noExtractHashtags": false,
#     "noExtractEmojis": false,
#     "fileIds": 
#     [

# 	"string"

#     ],
#     "mediaIds": 
#     [

# 	"string"

#     ],
#     "replyId": "string",
#     "renoteId": "string",
#     "channelId": "string",
#     "poll": 
#     {

# 	"choices": 

#         [],
#         "multiple": false,
#         "expiresAt": 0,
#         "expiredAfter": 1
#     }

# }

note=$(printf '{
  "i": "%s",
  "visibility": "%s",
  "text": %s,
  %s
  "localOnly": %s
}' "$auth" "$visibility" "$text" "$notecw" "$localOnly")

#echo "$note"

curl -X POST \
     --header "Content-Type: application/json; charset=UTF-8" \
     --data "$note" \
     https://"$host"/api/notes/create

printf "\n"
